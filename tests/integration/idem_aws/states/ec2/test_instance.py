import uuid

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_instance(hub, ctx, aws_ec2_subnet):
    # Create instance
    instance_temp_name = "idem-test-instance-" + str(uuid.uuid4())
    ret = await hub.states.aws.ec2.instance.present(
        ctx,
        name=instance_temp_name,
        subnet=aws_ec2_subnet.get("SubnetId"),
        tags=[{"Key": "Name", "Value": instance_temp_name}],
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("InstanceId")

    # Create instance again using the resource ID - expecting no 'changes'
    # Create instance with the same name will produce a new resource
    ret = await hub.states.aws.ec2.instance.present(
        ctx,
        name=resource_id,
        subnet=aws_ec2_subnet.get("SubnetId"),
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["new_state"].get("InstanceId", None) == resource_id, resource_id
    assert ret["changes"] == {}

    # Describe instance
    describe_ret = await hub.states.aws.ec2.instance.describe(ctx)
    assert resource_id in describe_ret

    # Delete instance
    ret = await hub.states.aws.ec2.instance.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
