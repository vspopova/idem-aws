import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_eks_addon(
    hub,
    ctx,
    aws_eks_cluster,
):
    if hub.tool.utils.is_running_localstack(ctx):
        return

    cluster_id = aws_eks_cluster.get("name")

    # create addons
    ret = await hub.states.aws.eks.addon.present(
        ctx,
        name="kube-proxy",
        cluster_name=cluster_id,
        addon_version="v1.21.2-eksbuild.2",
        tags={"Name": cluster_id},
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == f"Created 'kube-proxy'"
    resource = ret.get("new_state")
    addone1 = resource.get("addonName")

    # Describe kube-proxy addon
    describe_ret = await hub.states.aws.eks.addon.describe(ctx)
    assert addone1 in describe_ret

    # Delete addons
    ret = await hub.states.aws.eks.addon.absent(
        ctx, name="kube-proxy", cluster_name=cluster_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert ret["comment"] == f"Deleted 'kube-proxy'"
